# Create a VPC to launch our instances into
resource "aws_vpc" "default" {
  cidr_block = "${var.cidr}"

  tags {
    Name        = "${var.env_name}_VPC"
    Environment = "${var.env_name}"
  }
}

# Create an internet gateway to give our subnet access to the outside world
resource "aws_internet_gateway" "default" {
  vpc_id = "${aws_vpc.default.id}"

  tags {
    Environment = "${var.env_name}"
  }
}

# Create a private subnet
# resource "aws_subnet" "private_az_1" {
#   vpc_id            = "${aws_vpc.default.id}"
#   cidr_block        = "${var.cidr_private_subnet}"
#   availability_zone = "${var.aws_region}a"

#   tags {
#     Environment = "${var.env_name}"
#   }
# }

# Create a public subnet
resource "aws_subnet" "public_az_1" {
  vpc_id            = "${aws_vpc.default.id}"
  cidr_block        = "${var.cidr_public_subnet}"
  availability_zone = "${var.aws_region}a"

  tags {
    Environment = "${var.env_name}"
  }
}

# Internet route for public subnets
resource "aws_route_table" "internet" {
  vpc_id = "${aws_vpc.default.id}"

  route {
    cidr_block = "${var.anywhere}"
    gateway_id = "${aws_internet_gateway.default.id}"
  }

  tags {
    Environment = "${var.env_name}"
  }
}

# Associate the public subnet (zone A) with the IGW route table
resource "aws_route_table_association" "public_az_1" {
  subnet_id      = "${aws_subnet.public_az_1.id}"
  route_table_id = "${aws_route_table.internet.id}"
}
