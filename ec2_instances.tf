resource "aws_key_pair" "ec2_instances" {
  key_name   = "ec2_key"
  public_key = "${var.key}"
}

resource "aws_instance" "web_server" {
  ami                         = "ami-0a313d6098716f372"
  instance_type               = "t2.micro"
  key_name                    = "${aws_key_pair.ec2_instances.id}"
  vpc_security_group_ids      = ["${aws_security_group.ec2_instances.id}"]
  subnet_id                   = "${aws_subnet.public_az_1.id}"
  associate_public_ip_address = true

  root_block_device {
    volume_type           = "gp2"
    volume_size           = 20
    delete_on_termination = true
  }
  
  connection {
    private_key = "${file(var.private_key)}"
    user        = "${var.ansible_user}"
  }

  provisioner "remote-exec" {
    inline = ["sudo apt-get -qq install python -y"]
  }

  provisioner "local-exec" {
    command = <<EOT
      sleep 600;
	  >tomcat.ini;
	  echo "[tomcat]" | tee -a tomcat.ini;
	  echo "${aws_instance.web_server.public_ip} ansible_user=${var.ansible_user} ansible_ssh_private_key_file=${var.private_key}" | tee -a tomcat.ini;
      export ANSIBLE_HOST_KEY_CHECKING=False;
	  ansible-playbook -u ${var.ansible_user} --private-key ${var.private_key} -i tomcat.ini ../playbooks/deploy.yaml
    EOT
  }

  tags {
    Name      = "Tomcat"
    ManagedBy = "Terraform"
    Role      = "web"
  }
}
