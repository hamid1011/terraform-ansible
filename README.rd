# Command to use to run the terraform

While running the terraform, need to pass three things \n
    - aws_profile : Credential used to provision the resources on the cloud\n
    - aws_region  : Region that is being used to deploy the resources\n
    - key         : Public key to be passed, which correspondence private key will be used to ssh the ec2_instances. \n
\n
Example of this:\n
#terraform plan -var "aws_profile=profile_name" -var "aws_region=region_name" -var "key=Public-Key"