variable "aws_profile" {
  default     = "mylab"
  description = "Profile Used to provision the Infrastructure on AWS"
}

variable "aws_region" {
  description = "Region Used to deploy the services"
  default     = "us-east-1"
}

variable "key" {
  description = "Key To be used to login to the server"
  default     = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDuubp/HbOf71vV/wTZ743QUyE13/YJRVe+POlfQKaLDwPP/68l6LqmMlRp3uDf/wxyxMwH9UQhNU8qaZMvSbLe+GcrVTOTfwvBMJUgz1tqSpXEggi48x8OXd0uV1yJWV6zM+WHe3wK7pnmQPoXak6lfw7RGIEDi3SMQity6qb+xcjOZpf8yvmsWMudq/MBiUgCIf3xg9Dkx9alumvJFAKNWioJZIRaaDHvBKkIWM7LYVyy1y+6f6KJl73KULElKYQE2jB5OKEmq/6/fjkaeZFtGEje105H+DFceU/9leEfZbHvbncdopKqZWV2JcKaJ6qptnuWGzSdNe/DFWsxcJ/L"
}

variable "cidr" {
  description = "CIDR Block for the VPC"
  default     = "172.16.0.0/27"
}

# variable "cidr_private_subnet" {
#   description = "Private Subnet CIDR"
#   default     = "172.16.0.0/28"
# }

variable "cidr_public_subnet" {
  description = "Public Subnet CIDR"
  default     = "172.16.0.16/28"
}

variable "anywhere" {
  description = "Anywhere CIDR Notation"
  default     = "0.0.0.0/0"
}

variable "env_name" {
  description = "Name of the Environment"
  default     = "UAT"
}

variable "sg_name" {
  description = "Name of the Security Group"
  default     = "ecs_instances"
}

variable "ansible_user" {
  default = "ubuntu"
}

variable "private_key" {
  default = "ta"
}
